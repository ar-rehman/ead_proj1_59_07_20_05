﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TrendBirth
{
    [Table("Purchases")]
    public class PurchasesDTO
    {
        [Key]
        public int Purchase_ID { get; set; }
        public int Supplier_ID { get; set; }
        public int quantity { get; set; }
        public int stock_ID { get; set; }
    }
}
