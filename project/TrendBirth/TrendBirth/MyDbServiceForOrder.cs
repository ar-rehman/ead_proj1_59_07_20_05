﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TrendBirth
{
    public class MyDbServiceForOrder
    {

        MyDBContext dbctx;
        public MyDbServiceForOrder()
        {

            dbctx = new MyDBContext();
        }

        public void SaveOrder(OrderDTO dto)
        {
            dto.InProcess = false;
            dto.IsOut = false;
            dbctx.Order.Add(dto);
            dbctx.SaveChanges();
        }

        public void UpdateOrder(OrderDTO dto)
        {
            dbctx.Order.Attach(dto);
            var entry=dbctx.Entry(dto);
            entry.State = System.Data.Entity.EntityState.Modified;
            dbctx.SaveChanges();
        }

        public void UpdateOrderForInProcess(OrderDTO dto)
        {
            //dto.InProcess = true;
            //dbctx.Order.Attach(dto);
            //var entry = dbctx.Entry(dto);
            //dto.InProcess = true;
            //entry.State = System.Data.Entity.EntityState.Unchanged;
            //entry.Property(c => c.InProcess).IsModified = true;

            dto=dbctx.Order.Find(dto.Order_ID);
            dto.InProcess = true;
            dbctx.SaveChanges();
        }

        public void UpdateOrderForIsOut(OrderDTO dto)
        {
            dto.IsOut = true;
            dbctx.Order.Attach(dto);
            var entry = dbctx.Entry(dto);
            entry.State = System.Data.Entity.EntityState.Unchanged;
            entry.Property(c => c.IsOut).IsModified = true;
            dbctx.SaveChanges();
        }

        public OrderDTO getOrderByID(int id)
        {
            return dbctx.Order.Where(c => c.Order_ID == id).First();
        }

        public List<OrderDTO> getOrderByCustomerID(int id)
        {
            return dbctx.Order.Where(c => c.Customer_ID == id).ToList();
        }

        public List<OrderDTO> getAllOrder()
        {
            return dbctx.Order.ToList();
        }

        public OrderDTO getOrderByIGPNo(int id)
        {
            return dbctx.Order.Where(c => c.IGP_No == id).First();
        }


        public void SaveOrderProcess(OrderProcessDTO dto)
        {

            dbctx.OrderProcess.Add(dto);
            OrderDTO dto1 = new OrderDTO();
            dto1 = dbctx.Order.Find(dto.Order_ID);
            dto1.InProcess = true;
            dbctx.SaveChanges();

        }


        public void UpdateOrderProcess(OrderProcessDTO dto)
        {
            dbctx.OrderProcess.Attach(dto);
            var entry = dbctx.Entry(dto);
            entry.State = System.Data.Entity.EntityState.Modified;
            dbctx.SaveChanges();
        }

        public OrderProcessDTO getOrderProcessByID(int id)
        {
            return dbctx.OrderProcess.Where(c => c.OrderProcess_ID == id).First();
        }

        public List<OrderProcessDTO> getOrderProcessByOrderID(int id)
        {
            return dbctx.OrderProcess.Where(c => c.Order_ID == id).ToList();
        }

        public List<OrderProcessDTO> getOrderProcessByDate(DateTime dt)
        {
            return dbctx.OrderProcess.Where(c => c.DateTime == dt).ToList() ;
        }

        public List<OrderProcessDTO> getOrderProcessInProcess()
        {
            return dbctx.OrderProcess.Where(c => c.InProcess == true).ToList();
        }
        public void UpdateOrderProcessForInProcess(OrderProcessDTO dto)
        {
            dto.InProcess = false;
            dbctx.OrderProcess.Attach(dto);
            var entry = dbctx.Entry(dto);
            entry.State = System.Data.Entity.EntityState.Unchanged;
            entry.Property(c => c.InProcess).IsModified = true;
            dbctx.SaveChanges();
        }


        public void SaveOrderOut(OrderOutDTO dto)
        {
            dbctx.OrderOut.Add(dto);
            dbctx.SaveChanges();
        }

        public void UpdateOrderOut(OrderOutDTO dto)
        {
            dbctx.OrderOut.Attach(dto);
            var entry = dbctx.Entry(dto);
            entry.State = System.Data.Entity.EntityState.Modified;
            dbctx.SaveChanges();
        }



        public OrderOutDTO getOrderOutByID(int id)
        {
            
            return dbctx.OrderOut.Where(c => c.OrderOut_ID == id).First();
        }


        public int getAmountBuOrderId(int oid)
        {
           return  dbctx.OrderProcess.Where(c => c.Order_ID == oid).Select(c => c.Rate * c.DyingWeight).FirstOrDefault();
 
        }



        public List<completeOrderDTO> getAllCompletedOrders()
        {
            var query = from c in dbctx.Order
                        join d in dbctx.Customer on c.Customer_ID equals d.Customer_ID
                        where c.InProcess==true && c.IsOut==true
                        select new completeOrderDTO{ OrderId= c.Order_ID, CustomerId= d.Customer_ID,
                           CompanyName= d.CompanyName, Datetime=c.RecievedDateTime };


            return query.ToList();

        }


        public List<OrderOutDTO> getOrderOutByOrderProcessID(int id)
        {
            return dbctx.OrderOut.Where(c => c.OrderProcess_ID == id).ToList();
        }

        public void SaveStockManagement(Stock_ManagementDTO dto)
        {
            dbctx.Stock_Management.Add(dto);
            dbctx.SaveChanges();
        }

        public void UpdateStockManagement(Stock_ManagementDTO dto)
        {
            dbctx.Stock_Management.Attach(dto);
            var entry = dbctx.Entry(dto);
            entry.State = System.Data.Entity.EntityState.Modified;
            dbctx.SaveChanges();
        }
        
        public Stock_ManagementDTO GetStockManagementByID(int id)
        {
            return dbctx.Stock_Management.Where(c => c.StockDetail_ID == id).First();
        }

        public List<Stock_ManagementDTO> GetStockManagementByOrderID(int id)
        {
            return dbctx.Stock_Management.Where(c => c.Order_ID == id).ToList();
        }

        public List<Stock_ManagementDTO> GetStockManagementByItemID(int id)
        {
            return dbctx.Stock_Management.Where(c => c.ItemID == id).ToList();
        }

        public List<Stock_ManagementDTO> GetStockManagementByDate(DateTime dt)
        {
            return dbctx.Stock_Management.Where(c => c.DateTime == dt).ToList();
        }


    }
}
