﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TrendBirth
{
    public class MyDbServiceForStock
    {
        MyDBContext dbctx;
        public MyDbServiceForStock()
        {
            dbctx = new MyDBContext();
        }
        public void SaveStock(StockDTO dto)
        {
            dbctx.Stock.Add(dto);
            dbctx.SaveChanges();
        }

        public void UpdateStock(StockDTO dto)
        {
            dbctx.Stock.Attach(dto);
            var entry=dbctx.Entry(dto);
            entry.State = System.Data.Entity.EntityState.Modified;
            dbctx.SaveChanges();
        }

        public void IncrementStockById(int id,int inc)
        {
            var entry=dbctx.Stock.Where(c => c.ItemID == id).First();
            entry.ItemQty += inc;
            UpdateStock(entry);
        }

        public void DecrementStockById(int id, int inc)
        {
            var entry = dbctx.Stock.Where(c => c.ItemID == id).First();
            entry.ItemQty -= inc;
            UpdateStock(entry);
        }

        public void IncrementStockByName(String name, int inc)
        {
            var entry = dbctx.Stock.Where(c => c.ItemName == name).First();
            entry.ItemQty += inc;
            UpdateStock(entry);
        }

        public StockDTO GetStockById(int id)
        {
            return dbctx.Stock.Where(c => c.ItemID == id).First(); 
        }

        public StockDTO GetStockByName(String name)
        {
            return dbctx.Stock.Where(c => c.ItemName == name).First();
        }

        public List<StockDTO> GetAllStock()
        {
            return dbctx.Stock.ToList();
        }

        public void SavePurchase(PurchasesDTO dto)
        {
            dbctx.Purchases.Add(dto);
            dbctx.SaveChanges();
        }

        public void UpdatePurchases(PurchasesDTO dto)
        {
            dbctx.Purchases.Attach(dto);
            var entry =dbctx.Entry(dto);
            entry.State = System.Data.Entity.EntityState.Modified;
            dbctx.SaveChanges();
        }

        public PurchasesDTO getPurchasesByID(int id)
        {
            return dbctx.Purchases.Where(c => c.Purchase_ID == id).First();
        }

        public List<PurchasesDTO> GetPurchasesBySupplierID(int id)
        {
            return dbctx.Purchases.Where(c => c.Supplier_ID == id).ToList();
        }

        public List<PurchasesDTO> GetPurchasesByStockID(int id)
        {
            return dbctx.Purchases.Where(c => c.stock_ID == id).ToList();
        }

    }
}
