﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TrendBirth
{
    [Table("OrderOut")]
    public  class OrderOutDTO
    {
        [Key]
        public int OrderOut_ID { get; set; }
        public int OrderProcess_ID { get; set; }

        [Column("F_Weight")]
        public int F_Weight { get; set; }

        public int OGP { get; set; }

        public int NoOfRolls { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime DateTime { get; set; }
    
    }
}
