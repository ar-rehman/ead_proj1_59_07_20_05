﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TrendBirth
{
    [Table("CustomerCreditDebit") ]
    public class CustomerDebitCreditDTO
    {
        [Key]
        public int Transaction_ID { get; set; }
        public int Customer_ID { get; set; }
        public int Debit { get; set; }

        public int Credit { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime TransactionDate { get; set; }
        public string Description { get; set; }
       
    }
}
