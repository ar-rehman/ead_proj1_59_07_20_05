﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TrendBirth
{
    [Table("Invoice")]
    public    class InvoiceDTO
    {
            [Key]
        public int Invoice_ID { get; set; }

            public int OrderProcess_ID { get; set; }

            public int Customer_ID { get; set; }

            public int OrderOut_ID  { get; set; }
            public int Order_ID { get; set; }
            public int Recieved_Amount { get; set; }
            public DateTime DateTime { get; set; }

    }
}
