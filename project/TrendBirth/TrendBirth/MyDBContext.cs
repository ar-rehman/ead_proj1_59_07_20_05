﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace TrendBirth
{
    public class MyDBContext:DbContext
    {
        public MyDBContext():base("MyDBContext")
        {

        }

        public DbSet<UserDTO> User { get; set; }
        public DbSet<CustomerDTO> Customer { get; set; }
        public DbSet<CustomerDebitCreditDTO> CustomerDebitCredit { get; set; }
        public DbSet<EmployeeDTO> Employee { get; set; }
        public DbSet<InvoiceDTO> Invoice { get; set; }
        public DbSet<OrderDTO> Order { get; set; }
        public DbSet<OrderOutDTO> OrderOut { get; set; }
        public DbSet<OrderProcessDTO> OrderProcess { get; set; }
        public DbSet<PurchasesDTO> Purchases { get; set; }
        public DbSet<StockDTO> Stock { get; set; }
        public DbSet<Stock_ManagementDTO> Stock_Management { get; set; }
        public DbSet<SupplierDTO> Supplier { get; set; }
        public DbSet<SupplierDebitCreditDTO> SupplierDebitCredit { get; set; }
    }
}
