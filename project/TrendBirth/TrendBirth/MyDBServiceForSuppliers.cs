﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TrendBirth
{
    public class MyDBServiceForSuppliers
    {

                MyDBContext dbctx;
        public MyDBServiceForSuppliers()
        {
            dbctx=new MyDBContext();
        }

        public SupplierDTO GetSupplierByID(int id)
        {
            SupplierDTO dto = new SupplierDTO();
            dto = dbctx.Supplier.Where(c => c.Supplier_ID == id).First();
            return dto;

        }

        public SupplierDTO GetSupplierByName(String name)
        {
            SupplierDTO dto = new SupplierDTO();
            dto = dbctx.Supplier.Where(c => c.FirstName == name).First();
            return dto;
        }

        public List<DebitCreditView> GetDebitCreditOfCustomer()
        {
            var query = "execute dbo.SupplierDebitCreditview";
            var result = dbctx.Database.SqlQuery<DebitCreditView>(query).ToList();
            return result;

        }

        public List<SupplierDTO> GetAllSuppliers()
        {
            return dbctx.Supplier.ToList();
        }


        public void SaveSupplier(SupplierDTO dto)
        {
            dbctx.Supplier.Add(dto);
            dbctx.SaveChanges();

        }
        public void UpdateSupplier(SupplierDTO dto)
        {
            dbctx.Supplier.Attach(dto);
            var entry = dbctx.Entry(dto);
            entry.State = System.Data.Entity.EntityState.Modified;
            dbctx.SaveChanges();
        }
        public void DeleteSupplier(SupplierDTO dto)
        {
            dto.isActive = false;
            dbctx.Supplier.Attach(dto);
            var entry = dbctx.Entry(dto);
            entry.State = System.Data.Entity.EntityState.Unchanged;
            entry.Property(c => c.isActive).IsModified = true;
            dbctx.SaveChanges();

        }

        public void AddTransaction(SupplierDebitCreditDTO dto)
        {
            dbctx.SupplierDebitCredit.Add(dto);
            dbctx.SaveChanges();
        }

        public List<SupplierDebitCreditDTO> GetAllTransactions(SupplierDebitCreditDTO dto)
        {
            return dbctx.SupplierDebitCredit.ToList();
        }

        public int GetSupplierBalance(int id)
        {
            var totaldebit = dbctx.SupplierDebitCredit.Where(c => c.Supplier_ID == id).Sum(c => c.Debit);
            var totalcredit = dbctx.SupplierDebitCredit.Where(c => c.Supplier_ID == id).Sum(c => c.Credit);
            return totaldebit - totalcredit;
        }

    }
}
