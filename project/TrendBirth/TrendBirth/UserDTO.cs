﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrendBirth
{
    [Table("User")]
    public class UserDTO
    {
        [Key]
        public int User_ID { get; set; }
        public String UserName { get; set; }
        public String Password { get; set; }

    }
}
