﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TrendBirth
{
    [Table("Stock_Management")]
    public class Stock_ManagementDTO
    {
        [Key]
        public int StockDetail_ID { get; set; }
        public int ItemID { get; set; }
        public int Order_ID { get; set; }
        public int Quantity { get; set; }
        public DateTime DateTime { get; set; }


    }
}
