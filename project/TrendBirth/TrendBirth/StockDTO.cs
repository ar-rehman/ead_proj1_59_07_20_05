﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TrendBirth
{
    [Table("Stock")]
    public class StockDTO
    {
        [Key]
        public int ItemID { get; set; }
        public String ItemName { get; set; }
        public int ItemQty { get; set; }
        public DateTime LastIncModified { get; set; }
        public DateTime LastDecModified { get; set; }
    }
}
