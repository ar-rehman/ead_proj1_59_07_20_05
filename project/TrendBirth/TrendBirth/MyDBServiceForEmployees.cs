﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TrendBirth
{
    public class MyDBServiceForEmployees
    {
        MyDBContext dbctx;
        public MyDBServiceForEmployees()
        {
            dbctx=new MyDBContext();
        }

        public EmployeeDTO GetEmployeeByID(int id)
        {
            EmployeeDTO dto = new EmployeeDTO();
            dto = dbctx.Employee.Where(c => c.Employee_ID == id).First();
            return dto;

        }

        public EmployeeDTO GetEmployeeByName(String name)
        {
            EmployeeDTO dto = new EmployeeDTO();
            dto = dbctx.Employee.Where(c => c.FirstName == name).First();
            return dto;
        }

        public List<EmployeeDTO> GetAllEmployees()
        {
            return dbctx.Employee.ToList();
        }


        public void SaveEmployee(EmployeeDTO dto)
        {
            dbctx.Employee.Add(dto);
            dbctx.SaveChanges();

        }
        public void UpdateEmployee(EmployeeDTO dto)
        {
            dbctx.Employee.Attach(dto);
            var entry = dbctx.Entry(dto);
            entry.State = System.Data.Entity.EntityState.Modified;
            dbctx.SaveChanges();
        }
        public void DeleteEmployee(EmployeeDTO dto)
        {
            dto.isActive = false;
            dbctx.Employee.Attach(dto);
            var entry = dbctx.Entry(dto);
            entry.State = System.Data.Entity.EntityState.Unchanged;
            entry.Property(c => c.isActive).IsModified = true;
            dbctx.SaveChanges();

        }




    }
}
