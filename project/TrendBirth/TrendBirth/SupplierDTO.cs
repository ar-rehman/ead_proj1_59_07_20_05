﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TrendBirth
{
    [Table("Supplier")]
    public class SupplierDTO
    {
        [Key]
        public int Supplier_ID { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String PhoneNumber { get; set; }
        public String CompanyName { get; set; }
        public bool isActive { get; set; }

    }
}
