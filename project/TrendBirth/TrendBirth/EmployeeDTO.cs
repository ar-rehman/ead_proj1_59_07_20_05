﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TrendBirth
{
    [Table("Employee")]
    public class EmployeeDTO
    {
        [Key]
        public int Employee_ID { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String PhoneNumber { get; set; }
        public String Address { get; set; }
        public String CNIC { get; set; }
        public String Designation { get; set; }
        public int Salary { get; set; }

        public bool isActive { get; set; }
    }
}
