﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TrendBirth
{
    public class MyDbServiceForCustomers
    {

        MyDBContext dbctx;
        public MyDbServiceForCustomers()
        {

            dbctx = new MyDBContext();
        }


        public void SaveUser(UserDTO dto)
        {
            dbctx.User.Add(dto);
            dbctx.SaveChanges();
        }

        public UserDTO GetUserByName(String Name)
        {
            return dbctx.User.Where(c => c.UserName == Name).First(); 
        }

        public void UpdateUser(UserDTO dto)
        {
            dbctx.User.Attach(dto);
            var entry = dbctx.Entry(dto);
            entry.State = System.Data.Entity.EntityState.Modified;
            dbctx.SaveChanges();
        }

        public CustomerDTO GetCustomerByID(int id)
        {
            CustomerDTO dto = new CustomerDTO();
            dto=dbctx.Customer.Where(c => c.Customer_ID == id).First();
            return dto;

        }

        public CustomerDTO GetCustomerByName(String name)
        {
            CustomerDTO dto = new CustomerDTO();
            dto = dbctx.Customer.Where(c => c.FirstName == name).First();
            return dto;
        }

        public List<CustomerDTO> GetAllCustomers()
        {
            return dbctx.Customer.ToList();
        }


        public int SaveCustomer(CustomerDTO dto)
        {
            dbctx.Customer.Add(dto);
            dbctx.SaveChanges();
            return dto.Customer_ID;
        }
        public void  UpdateCustomer(CustomerDTO dto)
        {
            dbctx.Customer.Attach(dto);
            var entry = dbctx.Entry(dto);
            entry.State = System.Data.Entity.EntityState.Modified;
            dbctx.SaveChanges();
        }
        public void DeleteCustomer(CustomerDTO dto)
        {
            dto.isActive = false;
            dbctx.Customer.Attach(dto);
            var entry = dbctx.Entry(dto);
            entry.State = System.Data.Entity.EntityState.Unchanged;
            entry.Property(c => c.isActive).IsModified=true;
            dbctx.SaveChanges();

        }
         
        public void AddTransaction(CustomerDebitCreditDTO dto)
        {
            dbctx.CustomerDebitCredit.Add(dto);
            dbctx.SaveChanges();
        }

        public List<CustomerDebitCreditDTO> GetAllTransactions(CustomerDebitCreditDTO dto)
        {
            return dbctx.CustomerDebitCredit.ToList(); 
        }

        public int GetCustomerBalance(int id)
        {
            var totaldebit=dbctx.CustomerDebitCredit.Where(c => c.Customer_ID == id).Sum(c => c.Debit);
            var totalcredit = dbctx.CustomerDebitCredit.Where(c => c.Customer_ID == id).Sum(c => c.Credit);
            return totaldebit - totalcredit;
        }

        public List<DebitCreditView> GetDebitCreditOfCustomer()
        {
            var query = "execute dbo.DebitCreditView";
            var result = dbctx.Database.SqlQuery<DebitCreditView>(query).ToList();
            return result;

        }



        public void SaveInvoice(InvoiceDTO dto)
        {
            var query = String.Format("execute dbo.setInvoice {0} ,{1} ,{2}", dto.Order_ID, dto.Customer_ID, dto.Recieved_Amount);
            dbctx.Database.SqlQuery<int>(query);
        }

        public InvoiceDTO getInvoiceByID(int id)
        {
            return dbctx.Invoice.Where(c => c.Invoice_ID == id).First(); 
        }

        public List<InvoiceDTO> getInvoiceByCustomerID(int id)
        {
            return dbctx.Invoice.Where(c => c.Customer_ID == id).ToList();
        }

        public List<InvoiceDTO> getInvoiceByDate(DateTime dt)
        {
            return dbctx.Invoice.Where(c => c.DateTime == dt).ToList();
        }

//        public List<InvoiceDTO> getInvoicesOfLastMonth()
//        {
//            return dbctx.Invoice.Where(c=>c.DateTime>)
//        }





    }


}
