﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TrendBirth
{
    [Table("OrderProcess")]
    public class OrderProcessDTO
    {
        [Key]
        public int OrderProcess_ID { get; set; }
        public int Order_ID { get; set; }
        public int DyingWeight { get; set; }
        public int NoOfRolls { get; set; }
        public int Rate { get; set; }
        public int DyeUsed { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime DateTime { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public bool InProcess { get; set; }
    }
}
