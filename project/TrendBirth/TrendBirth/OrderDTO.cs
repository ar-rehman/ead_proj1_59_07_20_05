﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TrendBirth
{
    [Table("Order")]
    public class OrderDTO
    {
        [Key]
        public int Order_ID { get; set; }
        public int Customer_ID { get; set; }
        public String Agent { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime RecievedDateTime { get; set; }
        public int IGP_No   { get; set; }
        public int NoOfRolls { get; set; }
        public int Weight { get; set; }
        public String Color { get; set; }
        public bool InProcess { get; set; }
        public bool IsOut { get; set; }
    }
}
