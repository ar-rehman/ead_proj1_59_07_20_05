﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TrendBirth
{
    [Table("SupplierDebitCredit")]
    public class SupplierDebitCreditDTO
    {
        [Key]
        public int Transaction_ID { get; set; }
        public int Supplier_ID { get; set; }
        public int Debit { get; set; }
        public int Credit { get; set; }
        public int TransactionDateTime { get; set; }
        public String Description { get; set; }

    }
}
