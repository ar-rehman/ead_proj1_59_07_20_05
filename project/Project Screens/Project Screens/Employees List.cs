﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_Screens
{
    public partial class Employees_List : Form
    {
        int id;
        public Employees_List(int id1)
        {
            id = id1;
            
            InitializeComponent();
            dataGridView1.AutoGenerateColumns = false;
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Employees_List_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = TrendBirthBAL.GetAllEmployee();
        }

        private void MiniMize_BUnton_Click(object sender, EventArgs e)
        {

        }

        private void Clode_Button_Click(object sender, EventArgs e)
        {

        }

        private void panel2_MouseClick(object sender, MouseEventArgs e)
        {
            if (id == 0)
            {
                Form1 fr = new Form1();
                fr.Show();
                this.Hide();
            }
            else
            {
                List_View rc = new List_View(id);
                rc.Show();
                this.Hide();
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 7)
            {
                int empid = (int)dataGridView1.Rows[e.RowIndex].Cells[0].Value;
                Edit_Employee ed = new Edit_Employee(id,empid);
                ed.Show();
                this.Dispose();
            }
        }
    }
}
