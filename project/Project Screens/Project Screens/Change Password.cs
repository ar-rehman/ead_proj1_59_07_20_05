﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrendBirth;


namespace Project_Screens
{
    public partial class Change_Password : Form
    {
        public Change_Password()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            UserDTO dto = new UserDTO();
            dto.UserName = "Receptionist";
            dto.Password = textBox3.Text;
            TrendBirthBAL.UpdateUser(dto);
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void Change_Password_Load(object sender, EventArgs e)
        {
            UserDTO dto = new UserDTO();
            dto.UserName = "admin";
            UserDTO dto1 = new UserDTO();
            dto1.UserName = "Receptionist";
            dto=TrendBirthBAL.GetUserByName(dto);
            dto1 = TrendBirthBAL.GetUserByName(dto1);
            textBox1.Text = dto.Password;
            textBox2.Text = dto1.Password;
        }

        private void Admin_Login_Button_Click(object sender, EventArgs e)
        {
            UserDTO dto = new UserDTO();
            dto.UserName = "Admin";
            dto.Password = Admin_Pass.Text;
            TrendBirthBAL.UpdateUser(dto);
        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel4_MouseClick(object sender, MouseEventArgs e)
        {
            Form1 fm = new Form1();
            fm.Show();
            this.Dispose();
        }
    }
}
