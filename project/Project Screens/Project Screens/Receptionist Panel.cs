﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_Screens
{
    public partial class Receptionist_Panel : Form
    {
        int id;
        public Receptionist_Panel()
        {
            id = 1;
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void LogoutButton_Click(object sender, EventArgs e)
        {
            HomeScreen hm = new HomeScreen();
            hm.Show();
            this.Dispose();
        }

        private void panel4_MouseClick(object sender, MouseEventArgs e)
        {
            List_View ls = new List_View(id);
            ls.Show();
            this.Dispose();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel2_MouseClick(object sender, MouseEventArgs e)
        {
            Add_Customer ad = new Add_Customer(id);
            ad.Show();
            this.Hide();
        }

        private void panel6_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void panel6_MouseClick(object sender, MouseEventArgs e)
        {
            Add_Suplier ad = new Add_Suplier(id);
            ad.Show();
            this.Hide();
        }

        private void panel8_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel8_MouseClick(object sender, MouseEventArgs e)
        {
            CompletedOrders or = new CompletedOrders();
            or.Show();
            this.Dispose();

        }
    }
}
