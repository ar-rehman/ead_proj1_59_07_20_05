﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrendBirth;


namespace Project_Screens
{
    public partial class HomeScreen : Form
    {
        public HomeScreen()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void Admin_Text_Click(object sender, EventArgs e)
        {

        }

        private void Admin_Login_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Recceptionist_Pass_Enter(object sender, EventArgs e)
        {
            Recceptionist_Pass.Text = "";
        }

        private void Admin_Pass_Enter(object sender, EventArgs e)
        {
            Admin_Pass.Text = "";
        }

        private void Recceptionist_Pass_Leave(object sender, EventArgs e)
        {
            
        }

        private void Admin_Pass_Leave(object sender, EventArgs e)
        {
        }

        private void Clode_Button_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MiniMize_BUnton_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void Admin_Login_Button_Click(object sender, EventArgs e)
        {
            UserDTO dto = new UserDTO();
            dto.UserName = "admin";
            dto.Password = Admin_Pass.Text;
            int res=TrendBirthBAL.ValidateUserPass(dto);
            string show="";
            if (res == 0)
            {
                Form1 frm = new Form1();
                frm.Show();
                this.Hide();
 
            }
            else if(res==1)
            {
                show="Password Incorrect";
            }
            else if(res==2)
            {
                show = "User donot exist";
            }
            label2.Text=show;
        }

        private void Go_Recceptioist_Click(object sender, EventArgs e)
        {
            UserDTO dto = new UserDTO();
            dto.UserName = "Receptionist";
            dto.Password = Recceptionist_Pass.Text;
            int res = TrendBirthBAL.ValidateUserPass(dto);
            string show = "";
            if (res == 0)
            {

                Receptionist_Panel rec = new Receptionist_Panel();
                rec.Show();
                this.Hide();

            }
            else if (res == 1)
            {
                show = "Password Incorrect";
            }
            else if (res == 2)
            {
                show = "User donot exist";
            }
            label2.Text = show;
        }

        
    }
}
