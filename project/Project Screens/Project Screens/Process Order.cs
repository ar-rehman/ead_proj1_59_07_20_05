﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrendBirth;


namespace Project_Screens
{
    public partial class Process_Order : Form
    {
        int id, cid, oid;
        public Process_Order(int id1,int cid1,int oid1)
        {
            id = id1;
            cid = cid1;
            oid = oid1;
            InitializeComponent();
        }

        private void panel2_MouseClick(object sender, MouseEventArgs e)
        {
            Order_List_Recceptionist or = new Order_List_Recceptionist(id, cid);
            or.Show();
            this.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
        }

        private void Go_Recceptioist_Click(object sender, EventArgs e)
        {
            OrderProcessDTO dto = new OrderProcessDTO();
            dto.InProcess = true;
            dto.NoOfRolls = Int32.Parse( textBox2.Text);
            dto.DyingWeight = Int32.Parse( textBox3.Text);
            dto.DyeUsed = Int32.Parse(textBox6.Text);
            dto.Rate = Int32.Parse(textBox5.Text);
            dto.Order_ID = oid;
            
            MyDbServiceForOrder order = new MyDbServiceForOrder();
            order.SaveOrderProcess(dto);

        }
    }
}
