﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrendBirth;

namespace Project_Screens
{
    public partial class Add_Employee : Form
    {
        int id;
        public Add_Employee(int id1)
        {
            id = id1;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";
        }

        private void Go_Recceptioist_Click(object sender, EventArgs e)
        {
            EmployeeDTO dto = new EmployeeDTO();
            dto.FirstName = textBox1.Text;
            dto.LastName = textBox3.Text;
            dto.CNIC = textBox2.Text;
            dto.Address = textBox5.Text;
            dto.PhoneNumber = textBox4.Text;
            dto.Designation = textBox6.Text;
            dto.Salary = Int32.Parse( textBox7.Text);
            dto.isActive = true;
            TrendBirthBAL.SaveEmployee(dto);

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel2_MouseClick(object sender, MouseEventArgs e)
        {
            if (id == 0)
            {
                Form1 fm = new Form1();
                fm.Show();
                this.Dispose();
            }
        }
    }
}
