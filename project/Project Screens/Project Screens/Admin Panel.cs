﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_Screens
{
    public partial class Form1 : Form
    {
        int id;
        public Form1()
        {
            id = 0;
            InitializeComponent();
        }

        private void LogoutButton_Click(object sender, EventArgs e)
        {
            HomeScreen hm = new HomeScreen();
            hm.Show();
            this.Dispose();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void panel11_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel2_MouseClick(object sender, MouseEventArgs e)
        {
            Change_Password ch = new Change_Password();
            ch.Show();
            this.Dispose();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel1_MouseClick(object sender, MouseEventArgs e)
        {
            Add_Employee fm = new Add_Employee(id);
            fm.Show();
            this.Hide();
        }

        private void panel3_MouseClick(object sender, MouseEventArgs e)
        {
            Employees_List emp = new Employees_List(id);
            emp.Show();
            this.Hide();
        }

        private void panel6_MouseClick(object sender, MouseEventArgs e)
        {
            Inventory inv =new Inventory(id);
            inv.Show();
            this.Hide();

        }

        private void panel4_MouseClick(object sender, MouseEventArgs e)
        {
            Add_Suplier fr = new Add_Suplier(id);
            fr.Show();
            this.Hide();
        }

        private void panel9_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel9_MouseClick(object sender, MouseEventArgs e)
        {
            Add_Customer fr = new Add_Customer(id);
            fr.Show();
            this.Hide();
        }

        private void panel7_MouseClick(object sender, MouseEventArgs e)
        {
            Debit_Credit db = new Debit_Credit();
            db.Show();
            this.Hide();
        }

        private void panel8_MouseClick(object sender, MouseEventArgs e)
        {
            List_View v = new List_View(id);
            v.Show();
            this.Hide();
        }
    }
}
