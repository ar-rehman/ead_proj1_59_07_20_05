﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrendBirth;


namespace Project_Screens
{
    public partial class Order_in : Form
    {
        int id;
        int cid;
        public Order_in(int id1,int custid)
        {
            cid = custid;
            id = id1;
            InitializeComponent();
            textBox1.Text = cid.ToString();
            textBox1.ReadOnly=true;
        }

        private void panel2_MouseClick(object sender, MouseEventArgs e)
        {
            Order_List_Recceptionist ls = new Order_List_Recceptionist(id,cid);
            ls.Show();
            this.Dispose();
        }

        private void Order_in_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";

        }

        private void Go_Recceptioist_Click(object sender, EventArgs e)
        {
            OrderDTO dto = new OrderDTO();
            dto.Customer_ID = cid;
            dto.InProcess = false;
            dto.IsOut = false;
            dto.NoOfRolls = Int32.Parse(textBox6.Text);
            dto.Agent = textBox3.Text;
            dto.IGP_No = Int32.Parse(textBox2.Text) ;
            dto.NoOfRolls = Int32.Parse(textBox6.Text) ;
            dto.Weight = Int32.Parse(textBox5.Text);
            dto.Color = textBox4.Text;
            MyDbServiceForOrder order = new MyDbServiceForOrder();
            order.SaveOrder(dto);
        }
    }
}
