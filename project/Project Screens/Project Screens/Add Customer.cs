﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrendBirth;

namespace Project_Screens
{
    public partial class Add_Customer : Form
    {
        int id;
        public Add_Customer(int id1)
        {
            InitializeComponent();
            id = id1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox5.Text = "";
        }

        private void Go_Recceptioist_Click(object sender, EventArgs e)
        {
            CustomerDTO dto = new CustomerDTO();
            dto.FirstName = textBox1.Text;
            dto.LastName = textBox3.Text;
            dto.PhoneNumber = textBox2.Text;
            dto.CompanyName = textBox5.Text;
            dto.isActive = true;
            TrendBirthBAL.SaveCustomer(dto);
        }

        private void panel2_MouseClick(object sender, MouseEventArgs e)
        {
            if (id == 0)
            {
                Form1 fm = new Form1();
                fm.Show();
                this.Dispose();
            }
            else
            {
                Receptionist_Panel fm = new Receptionist_Panel();
                fm.Show();
                this.Dispose();
            }
        }
    }
}
