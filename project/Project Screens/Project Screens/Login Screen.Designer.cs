﻿namespace Project_Screens
{
    partial class HomeScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HomeScreen));
            this.Logo = new System.Windows.Forms.Panel();
            this.Title = new System.Windows.Forms.Label();
            this.Clode_Button = new System.Windows.Forms.Label();
            this.MiniMize_BUnton = new System.Windows.Forms.Label();
            this.Admin_Login = new System.Windows.Forms.Panel();
            this.Admin_Text = new System.Windows.Forms.Label();
            this.Admin_Pass = new System.Windows.Forms.TextBox();
            this.Admin_Login_Button = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.Recceptionist_Pass = new System.Windows.Forms.TextBox();
            this.Go_Recceptioist = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Logo
            // 
            this.Logo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Logo.BackgroundImage")));
            this.Logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Logo.Location = new System.Drawing.Point(-1, 1);
            this.Logo.Name = "Logo";
            this.Logo.Size = new System.Drawing.Size(79, 44);
            this.Logo.TabIndex = 0;
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.ForeColor = System.Drawing.Color.White;
            this.Title.Location = new System.Drawing.Point(77, 15);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(410, 31);
            this.Title.TabIndex = 1;
            this.Title.Text = "Trend Birth Management System";
            this.Title.Click += new System.EventHandler(this.label1_Click);
            // 
            // Clode_Button
            // 
            this.Clode_Button.AutoSize = true;
            this.Clode_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Clode_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Clode_Button.ForeColor = System.Drawing.Color.White;
            this.Clode_Button.Location = new System.Drawing.Point(693, 14);
            this.Clode_Button.Name = "Clode_Button";
            this.Clode_Button.Size = new System.Drawing.Size(24, 24);
            this.Clode_Button.TabIndex = 2;
            this.Clode_Button.Text = "X";
            this.Clode_Button.Click += new System.EventHandler(this.Clode_Button_Click);
            // 
            // MiniMize_BUnton
            // 
            this.MiniMize_BUnton.AutoSize = true;
            this.MiniMize_BUnton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MiniMize_BUnton.ForeColor = System.Drawing.Color.White;
            this.MiniMize_BUnton.Location = new System.Drawing.Point(673, 11);
            this.MiniMize_BUnton.Name = "MiniMize_BUnton";
            this.MiniMize_BUnton.Size = new System.Drawing.Size(21, 24);
            this.MiniMize_BUnton.TabIndex = 3;
            this.MiniMize_BUnton.Text = "_";
            this.MiniMize_BUnton.Click += new System.EventHandler(this.MiniMize_BUnton_Click);
            // 
            // Admin_Login
            // 
            this.Admin_Login.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Admin_Login.BackgroundImage")));
            this.Admin_Login.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Admin_Login.Location = new System.Drawing.Point(111, 72);
            this.Admin_Login.Name = "Admin_Login";
            this.Admin_Login.Size = new System.Drawing.Size(167, 166);
            this.Admin_Login.TabIndex = 4;
            this.Admin_Login.Paint += new System.Windows.Forms.PaintEventHandler(this.Admin_Login_Paint);
            // 
            // Admin_Text
            // 
            this.Admin_Text.AutoSize = true;
            this.Admin_Text.BackColor = System.Drawing.Color.Transparent;
            this.Admin_Text.Font = new System.Drawing.Font("Trebuchet MS", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Admin_Text.ForeColor = System.Drawing.Color.White;
            this.Admin_Text.Location = new System.Drawing.Point(108, 243);
            this.Admin_Text.Name = "Admin_Text";
            this.Admin_Text.Size = new System.Drawing.Size(172, 35);
            this.Admin_Text.TabIndex = 5;
            this.Admin_Text.Text = "Admin Login";
            this.Admin_Text.Click += new System.EventHandler(this.Admin_Text_Click);
            // 
            // Admin_Pass
            // 
            this.Admin_Pass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Admin_Pass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Admin_Pass.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Admin_Pass.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.Admin_Pass.Location = new System.Drawing.Point(82, 281);
            this.Admin_Pass.Name = "Admin_Pass";
            this.Admin_Pass.PasswordChar = 'X';
            this.Admin_Pass.Size = new System.Drawing.Size(219, 26);
            this.Admin_Pass.TabIndex = 6;
            this.Admin_Pass.Text = " ";
            this.Admin_Pass.Enter += new System.EventHandler(this.Admin_Pass_Enter);
            this.Admin_Pass.Leave += new System.EventHandler(this.Admin_Pass_Leave);
            // 
            // Admin_Login_Button
            // 
            this.Admin_Login_Button.BackColor = System.Drawing.Color.DimGray;
            this.Admin_Login_Button.Cursor = System.Windows.Forms.Cursors.Default;
            this.Admin_Login_Button.FlatAppearance.BorderSize = 0;
            this.Admin_Login_Button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Azure;
            this.Admin_Login_Button.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue;
            this.Admin_Login_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Admin_Login_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Admin_Login_Button.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.Admin_Login_Button.Location = new System.Drawing.Point(82, 316);
            this.Admin_Login_Button.Name = "Admin_Login_Button";
            this.Admin_Login_Button.Size = new System.Drawing.Size(219, 31);
            this.Admin_Login_Button.TabIndex = 7;
            this.Admin_Login_Button.Text = "Go Admin";
            this.Admin_Login_Button.UseVisualStyleBackColor = false;
            this.Admin_Login_Button.Click += new System.EventHandler(this.Admin_Login_Button_Click);
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel1.Location = new System.Drawing.Point(448, 74);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(161, 160);
            this.panel1.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Trebuchet MS", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(437, 241);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(183, 35);
            this.label1.TabIndex = 9;
            this.label1.Text = "Receptionist ";
            // 
            // Recceptionist_Pass
            // 
            this.Recceptionist_Pass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Recceptionist_Pass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Recceptionist_Pass.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Recceptionist_Pass.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.Recceptionist_Pass.Location = new System.Drawing.Point(415, 281);
            this.Recceptionist_Pass.Name = "Recceptionist_Pass";
            this.Recceptionist_Pass.Size = new System.Drawing.Size(219, 26);
            this.Recceptionist_Pass.TabIndex = 10;
            this.Recceptionist_Pass.Enter += new System.EventHandler(this.Recceptionist_Pass_Enter);
            this.Recceptionist_Pass.Leave += new System.EventHandler(this.Recceptionist_Pass_Leave);
            // 
            // Go_Recceptioist
            // 
            this.Go_Recceptioist.BackColor = System.Drawing.Color.DimGray;
            this.Go_Recceptioist.Cursor = System.Windows.Forms.Cursors.Default;
            this.Go_Recceptioist.FlatAppearance.BorderSize = 0;
            this.Go_Recceptioist.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Azure;
            this.Go_Recceptioist.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkOrange;
            this.Go_Recceptioist.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Go_Recceptioist.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Go_Recceptioist.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.Go_Recceptioist.Location = new System.Drawing.Point(415, 316);
            this.Go_Recceptioist.Name = "Go_Recceptioist";
            this.Go_Recceptioist.Size = new System.Drawing.Size(219, 31);
            this.Go_Recceptioist.TabIndex = 11;
            this.Go_Recceptioist.Text = "Go Receptionist";
            this.Go_Recceptioist.UseVisualStyleBackColor = false;
            this.Go_Recceptioist.Click += new System.EventHandler(this.Go_Recceptioist_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(295, 258);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 12;
            // 
            // HomeScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(732, 409);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Go_Recceptioist);
            this.Controls.Add(this.Recceptionist_Pass);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.Admin_Login_Button);
            this.Controls.Add(this.Admin_Pass);
            this.Controls.Add(this.Admin_Text);
            this.Controls.Add(this.Admin_Login);
            this.Controls.Add(this.MiniMize_BUnton);
            this.Controls.Add(this.Clode_Button);
            this.Controls.Add(this.Title);
            this.Controls.Add(this.Logo);
            this.ForeColor = System.Drawing.Color.Red;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "HomeScreen";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Logo;
        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Label Clode_Button;
        private System.Windows.Forms.Label MiniMize_BUnton;
        private System.Windows.Forms.Panel Admin_Login;
        private System.Windows.Forms.Label Admin_Text;
        private System.Windows.Forms.TextBox Admin_Pass;
        private System.Windows.Forms.Button Admin_Login_Button;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Recceptionist_Pass;
        private System.Windows.Forms.Button Go_Recceptioist;
        private System.Windows.Forms.Label label2;
    }
}

