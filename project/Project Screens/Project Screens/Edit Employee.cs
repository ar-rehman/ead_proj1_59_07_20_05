﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrendBirth;

namespace Project_Screens
{
    public partial class Edit_Employee : Form
    {
        int id;
        EmployeeDTO dto;
        public Edit_Employee(int id1,int empid)
        {
            id = id1;
            dto=TrendBirthBAL.GetEmployeeByID(empid);
            InitializeComponent();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel2_MouseClick(object sender, MouseEventArgs e)
        {
            Employees_List fm = new Employees_List(id);
            fm.Show();
            this.Dispose();
        }

        private void Edit_Employee_Load(object sender, EventArgs e)
        {
            textBox1.Text = dto.FirstName;
            textBox3.Text = dto.LastName;
            textBox2.Text = dto.CNIC;
            textBox5.Text = dto.Address;
            textBox4.Text = dto.PhoneNumber.ToString();
            textBox6.Text = dto.Designation;
            textBox7.Text = dto.Salary.ToString();
        }

        private void Go_Recceptioist_Click(object sender, EventArgs e)
        {
            dto.FirstName = textBox1.Text;
            dto.LastName = textBox3.Text;
            dto.CNIC = textBox2.Text;
            dto.Address = textBox5.Text;
            dto.PhoneNumber = textBox4.Text;
            dto.Designation = textBox6.Text;
            dto.Salary = Int32.Parse(textBox7.Text);
            TrendBirthBAL.UpdateEmployee(dto);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
