﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrendBirth;


namespace Project_Screens
{
    public partial class Order_CheckOut : Form
    {
        int id;
        int cid;
        int oid;

        public Order_CheckOut(int id1,int cid1,int oid1)
        {
            id = id1;
            cid = cid1;
            oid = oid1;
            InitializeComponent();
        }

        private void panel2_MouseClick(object sender, MouseEventArgs e)
        {
            Order_List_Recceptionist or = new Order_List_Recceptionist(id,cid);
            or.Show();
            this.Dispose();
        }

        private void Order_CheckOut_Load(object sender, EventArgs e)
        {
            textBox1.Text = oid.ToString();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox6.Text = "";
        }

        private void Go_Recceptioist_Click(object sender, EventArgs e)
        {
            OrderOutDTO dto = new OrderOutDTO();
            dto.F_Weight = Int32.Parse( textBox3.Text);

            dto.OGP = Int32.Parse( textBox6.Text);
            dto.NoOfRolls = Int32.Parse(textBox2.Text);
            MyDbServiceForOrder order = new MyDbServiceForOrder();
            List<OrderProcessDTO> dt= order.getOrderProcessByOrderID(oid);
            dto.OrderProcess_ID = dt[0].OrderProcess_ID;
            order.SaveOrderOut(dto);
            OrderDTO dt1 = new OrderDTO();
            dt1.Order_ID = oid;
            order.UpdateOrderForIsOut(dt1);
            MesssageBox ms = new MesssageBox();
            ms.ShowDialog();
            Order_List_Recceptionist or = new Order_List_Recceptionist(id,cid);
            or.Show();
            this.Dispose();
            
        }

    }
}
