﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrendBirth;
namespace Project_Screens
{
    public partial class Order_List_Recceptionist : Form
    {
        int id;
        int cid;
        public Order_List_Recceptionist(int id1,int custid)
        {
            cid = custid;
            id = id1;
            InitializeComponent();
            dataGridView2.AutoGenerateColumns = false;
        }

        private void Order_List_Recceptionist_Load(object sender, EventArgs e)
        {
            MyDbServiceForOrder or = new MyDbServiceForOrder();
            if (cid > 0)
                dataGridView2.DataSource = or.getOrderByCustomerID(cid);
            else
                dataGridView2.DataSource = or.getAllOrder();

        }

        private void panel2_MouseClick(object sender, MouseEventArgs e)
        {
            Customer_List cl = new Customer_List(id);
            cl.Show();
            this.Dispose();
        }

        private void label1_MouseClick(object sender, MouseEventArgs e)
        {
            Order_in or = new Order_in(id, cid);
            or.Show();
            this.Dispose();
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex==10)
            {
                int oid = (int)dataGridView2.Rows[e.RowIndex].Cells[0].Value ;
                Process_Order pr = new Process_Order(id, cid, oid);
                pr.Show();
                this.Dispose();
            }
            else if (e.ColumnIndex == 11)
            {
                int oid = (int)dataGridView2.Rows[e.RowIndex].Cells[0].Value;
                Order_CheckOut pr = new Order_CheckOut(id, cid, oid);
                pr.Show();
                this.Dispose();
            }

        }
    }
}
