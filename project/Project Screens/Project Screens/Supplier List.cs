﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_Screens
{
    public partial class Supplier_List : Form
    {
        int id;
        public Supplier_List(int id1)
        {
            id = id1;
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void panel2_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }

        private void panel2_MouseClick(object sender, MouseEventArgs e)
        {
            List_View ls = new List_View(id);
            ls.Show();
            this.Hide();
        }

        private void label1_MouseClick(object sender, MouseEventArgs e)
        {
            Add_Suplier ad = new Add_Suplier(id);
            ad.Show();
            this.Hide();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Supplier_List_Load(object sender, EventArgs e)
        {
            dataGridView2.DataSource = TrendBirthBAL.GetAllSupplier();
        }
    }
}
