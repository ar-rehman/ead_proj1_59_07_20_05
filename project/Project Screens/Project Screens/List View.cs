﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_Screens
{
    public partial class List_View : Form
    {
        int id;
        public List_View(int id1)
        {
            id = id1;
            InitializeComponent();
        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel2_MouseClick(object sender, MouseEventArgs e)
        {
            if (id == 0)
            {
                Form1 f = new Form1();
                f.Show();
                this.Hide();
            }
            else
            {
                Receptionist_Panel re = new Receptionist_Panel();
                re.Show();
                this.Dispose();
            }
        }

        private void panel1_MouseClick(object sender, MouseEventArgs e)
        {
            Employees_List eq = new Employees_List(id);
            eq.Show();
            this.Hide();
        }

        private void panel9_MouseClick(object sender, MouseEventArgs e)
        {
            Customer_List c = new Customer_List(id);
            c.Show();
            this.Hide();
        }

        private void panel4_MouseClick(object sender, MouseEventArgs e)
        {
            Supplier_List s = new Supplier_List(id);
            s.Show();
            this.Hide();
        }

        private void panel3_MouseClick(object sender, MouseEventArgs e)
        {
            if (id == 0)
            {
                Order_List or = new Order_List(id,0);
                or.Show();
                this.Dispose();
            }
            else
            {
                Order_List or = new Order_List(id,0);
                or.Show();
                this.Dispose();
            }
                
        }

        private void List_View_Load(object sender, EventArgs e)
        {

        }
    }
}
