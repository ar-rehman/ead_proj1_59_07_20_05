﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_Screens
{
    public partial class Customer_List : Form
    {
        int id;
        public Customer_List(int id1)
        {
            id = id1;
            InitializeComponent();
            dataGridView2.AutoGenerateColumns = false;

        }

        private void panel2_MouseClick(object sender, MouseEventArgs e)
        {
            List_View l = new List_View(id);
            l.Show();
            this.Hide();
        }

        private void Customer_List_Load(object sender, EventArgs e)
        {
            dataGridView2.DataSource = TrendBirthBAL.GetAllCustomers();
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex==5)
            {
                int cid = (int)dataGridView2.Rows[e.RowIndex].Cells[0].Value;
                if (id == 0)
                {
                    Order_List or = new Order_List(id, cid);
                    or.Show();
                    this.Dispose();
                }
                else
                {
                    Order_List_Recceptionist or = new Order_List_Recceptionist(id, cid);
                    or.Show();
                    this.Dispose();
                }
            }
        }

        private void label1_MouseClick(object sender, MouseEventArgs e)
        {
            Add_Customer ad = new Add_Customer(id);
            ad.Show();
            this.Dispose();
        }
    }
}
