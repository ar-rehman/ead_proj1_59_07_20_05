﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_Screens
{
    public partial class Debit_Credit : Form
    {
        public Debit_Credit()
        {
            InitializeComponent();
        }

        private void Debit_Credit_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = TrendBirthBAL.getDebitCreditViewOfCustomer();
            dataGridView2.DataSource = TrendBirthBAL.getDebitCreditViewOfSupplier();
        }

        private void panel3_MouseClick(object sender, MouseEventArgs e)
        {
            Form1 fr = new Form1();
            fr.Show();
            this.Hide();
        }
    }
}
