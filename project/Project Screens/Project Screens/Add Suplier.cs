﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrendBirth;

namespace Project_Screens
{
    public partial class Add_Suplier : Form
    {
        int id;
        public Add_Suplier(int id1)
        {
            id = id1;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox5.Text = "";
        }

        private void Go_Recceptioist_Click(object sender, EventArgs e)
        {
            SupplierDTO dto = new SupplierDTO();
            dto.FirstName = textBox1.Text;
            dto.LastName = textBox3.Text;
            dto.PhoneNumber = textBox2.Text;
            dto.CompanyName = textBox5.Text;
            dto.isActive = true;
            TrendBirthBAL.SaveSupplier(dto);
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel2_MouseClick(object sender, MouseEventArgs e)
        {
            if (id == 0)
            {
                Form1 fm = new Form1();
                fm.Show();
                this.Dispose();
            }
            else
            {
                Receptionist_Panel re = new Receptionist_Panel();
                re.Show();
                this.Dispose();
            }
        }

        private void Add_Suplier_Load(object sender, EventArgs e)
        {

        }

        
    }
}
