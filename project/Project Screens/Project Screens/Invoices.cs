﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrendBirth;

namespace Project_Screens
{
    public partial class Invoices : Form
    {
        int oid, cid;
        public Invoices(int oid1,int cid1)
        {
            oid = oid1;
            cid = cid1;
            InitializeComponent();
            textBox1.Text = oid.ToString();
            textBox2.Text = cid.ToString();
            textBox1.ReadOnly=true;
            textBox2.ReadOnly = true;
            MyDbServiceForOrder o=new MyDbServiceForOrder();

            textBox3.Text = o.getAmountBuOrderId(oid).ToString();
            textBox3.ReadOnly = true;
            textBox5.ReadOnly = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            textBox3.Text = textBox4.Text = textBox5.Text = "";
        }

        private void Go_Recceptioist_Click(object sender, EventArgs e)
        {
            MyDbServiceForCustomers or = new MyDbServiceForCustomers();
            InvoiceDTO dto=new InvoiceDTO();
            dto.Customer_ID=cid;
            dto.Order_ID = oid;
            dto.Recieved_Amount = Int32.Parse(textBox4.Text);
            or.SaveInvoice(dto);
            CustomerDebitCreditDTO dt=new CustomerDebitCreditDTO();
            dt.Customer_ID=cid;
            dt.Credit=dto.Recieved_Amount;
            dt.Debit= Int32.Parse( textBox3.Text);
            dt.Description="";
            or.AddTransaction(dt);
        }
    }
}
