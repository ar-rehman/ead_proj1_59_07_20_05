﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrendBirth;

namespace Project_Screens
{
    public partial class CompletedOrders : Form
    {
        public CompletedOrders()
        {
            InitializeComponent();
            dataGridView1.AutoGenerateColumns = false;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                int oid = (int)dataGridView1.Rows[e.RowIndex].Cells[0].Value;
                int cid = (int)dataGridView1.Rows[e.RowIndex].Cells[1].Value;

                Invoices inv = new Invoices(oid,cid);
                inv.Show();
                this.Dispose();
                    
 
            }
        }

        private void CompletedOrders_Load(object sender, EventArgs e)
        {
            MyDbServiceForOrder db = new MyDbServiceForOrder();
           dataGridView1.DataSource =  db.getAllCompletedOrders();  
        }

        private void panel1_MouseClick(object sender, MouseEventArgs e)
        {
            Receptionist_Panel r = new Receptionist_Panel();
            r.Show();
            this.Dispose();
        }
    }
}
