﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_Screens
{
    public partial class Order_List : Form
    {
        int id;
        int cid;
        public Order_List(int id1,int custid)
        {
            InitializeComponent();
            id = id1;
            cid = custid;
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void MiniMize_BUnton_Click(object sender, EventArgs e)
        {

        }

        private void Clode_Button_Click(object sender, EventArgs e)
        {

        }

        private void Title_Click(object sender, EventArgs e)
        {

        }

        private void Logo_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void panel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void panel2_MouseClick(object sender, MouseEventArgs e)
        {
            List_View ls = new List_View(id);
            ls.Show();
            this.Dispose();
        }

        private void Order_List_Load(object sender, EventArgs e)
        {
            if(cid>0)
                dataGridView2.DataSource = TrendBirthBAL.GetOrderByCustomerId(id);
            else
                dataGridView2.DataSource = TrendBirthBAL.GetAllOrders();
        }
    }
}
