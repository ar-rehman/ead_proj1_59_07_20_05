﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrendBirth;
namespace Project_Screens
{

    public static class TrendBirthBAL
    {
        public static MyDbServiceForCustomers cust=new MyDbServiceForCustomers();
        public static MyDBServiceForEmployees emp = new MyDBServiceForEmployees();
        public static MyDBServiceForSuppliers sup = new MyDBServiceForSuppliers();
        public static MyDbServiceForOrder order = new MyDbServiceForOrder();
        public static MyDbServiceForStock stock = new MyDbServiceForStock();
        
        public static int ValidateUserPass(UserDTO dto)
        {
            UserDTO d1=cust.GetUserByName(dto.UserName);
            if(d1==null)
            {
                return 2;
            }
            if (d1.Password.Equals(dto.Password))
            {
                return 0;
            }
            else
                return 1;

        }
        public static UserDTO GetUserByName(UserDTO dto)
        {
            UserDTO d1 = cust.GetUserByName(dto.UserName);
            return d1;
        }

        public static void UpdateUser(UserDTO dto)
        {
            UserDTO d1 = cust.GetUserByName(dto.UserName);
            d1.Password = dto.Password;
            cust.UpdateUser(d1);
        }
        public static void SaveEmployee(EmployeeDTO dto)
        {
            emp.SaveEmployee(dto);
        }

        public static EmployeeDTO GetEmployeeByID(int id)
        {
            return emp.GetEmployeeByID(id);
        }

        public static void UpdateEmployee(EmployeeDTO dto)
        {
            emp.UpdateEmployee(dto);
        }

        public static void SaveSupplier(SupplierDTO dto)
        {
            sup.SaveSupplier(dto);
        }

        internal static void SaveCustomer(CustomerDTO dto)
        {
            cust.SaveCustomer(dto);
        }

        internal static List<EmployeeDTO> GetAllEmployee()
        {
            return emp.GetAllEmployees();
        }

        internal static object getDebitCreditViewOfCustomer()
        {
            return cust.GetDebitCreditOfCustomer();
        }

        internal static object getDebitCreditViewOfSupplier()
        {
            return sup.GetDebitCreditOfCustomer();
        }

        internal static object GetAllCustomers()
        {

            return cust.GetAllCustomers();

        }

        internal static object GetAllSupplier()
        {
            return sup.GetAllSuppliers();
        }

        internal static object GetAllOrders()
        {
            return order.getAllOrder();
        }

        internal static object GetOrderByCustomerId(int id)
        {
            
            return order.getOrderByCustomerID(id);

        }

        internal static object GetAllInventory()
        {
            return stock.GetAllStock();

        }
    }
}
