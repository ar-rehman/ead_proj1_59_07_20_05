﻿namespace Project_Screens
{
    partial class Order_List_Recceptionist
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Order_List_Recceptionist));
            this.MiniMize_BUnton = new System.Windows.Forms.Label();
            this.Clode_Button = new System.Windows.Forms.Label();
            this.Title = new System.Windows.Forms.Label();
            this.Logo = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.OrderID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Customer_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Agent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReceivedDateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IGPNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoOfRolls = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Weight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Color = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InProcess = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsOut = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ViewOrder = new System.Windows.Forms.DataGridViewButtonColumn();
            this.GoOrderOut = new System.Windows.Forms.DataGridViewButtonColumn();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // MiniMize_BUnton
            // 
            this.MiniMize_BUnton.AutoSize = true;
            this.MiniMize_BUnton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MiniMize_BUnton.ForeColor = System.Drawing.Color.White;
            this.MiniMize_BUnton.Location = new System.Drawing.Point(1107, 6);
            this.MiniMize_BUnton.Name = "MiniMize_BUnton";
            this.MiniMize_BUnton.Size = new System.Drawing.Size(21, 24);
            this.MiniMize_BUnton.TabIndex = 30;
            this.MiniMize_BUnton.Text = "_";
            // 
            // Clode_Button
            // 
            this.Clode_Button.AutoSize = true;
            this.Clode_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Clode_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Clode_Button.ForeColor = System.Drawing.Color.White;
            this.Clode_Button.Location = new System.Drawing.Point(1127, 9);
            this.Clode_Button.Name = "Clode_Button";
            this.Clode_Button.Size = new System.Drawing.Size(24, 24);
            this.Clode_Button.TabIndex = 29;
            this.Clode_Button.Text = "X";
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.ForeColor = System.Drawing.Color.White;
            this.Title.Location = new System.Drawing.Point(75, 14);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(410, 31);
            this.Title.TabIndex = 28;
            this.Title.Text = "Trend Birth Management System";
            // 
            // Logo
            // 
            this.Logo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Logo.BackgroundImage")));
            this.Logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Logo.Location = new System.Drawing.Point(-3, 0);
            this.Logo.Name = "Logo";
            this.Logo.Size = new System.Drawing.Size(79, 44);
            this.Logo.TabIndex = 27;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Crimson;
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.panel2);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Location = new System.Drawing.Point(12, 51);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1136, 89);
            this.panel3.TabIndex = 31;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 58F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(914, -5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 94);
            this.label1.TabIndex = 9;
            this.label1.Text = "+";
            this.label1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.label1_MouseClick);
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel2.Location = new System.Drawing.Point(1066, 19);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(48, 51);
            this.panel2.TabIndex = 8;
            this.panel2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(77, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(178, 45);
            this.label2.TabIndex = 1;
            this.label2.Text = "Order List";
            // 
            // panel5
            // 
            this.panel5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel5.BackgroundImage")));
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel5.Location = new System.Drawing.Point(13, 14);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(58, 58);
            this.panel5.TabIndex = 0;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.OrderID,
            this.Customer_ID,
            this.Agent,
            this.ReceivedDateTime,
            this.IGPNo,
            this.NoOfRolls,
            this.Weight,
            this.Color,
            this.InProcess,
            this.IsOut,
            this.ViewOrder,
            this.GoOrderOut});
            this.dataGridView2.Location = new System.Drawing.Point(12, 162);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.Size = new System.Drawing.Size(1141, 352);
            this.dataGridView2.TabIndex = 2;
            this.dataGridView2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellContentClick);
            // 
            // OrderID
            // 
            this.OrderID.DataPropertyName = "Order_ID";
            this.OrderID.HeaderText = "OrderID";
            this.OrderID.Name = "OrderID";
            this.OrderID.ReadOnly = true;
            // 
            // Customer_ID
            // 
            this.Customer_ID.DataPropertyName = "Customer_ID";
            this.Customer_ID.HeaderText = "Customer_ID";
            this.Customer_ID.Name = "Customer_ID";
            this.Customer_ID.ReadOnly = true;
            // 
            // Agent
            // 
            this.Agent.DataPropertyName = "Agent";
            this.Agent.HeaderText = "Agent";
            this.Agent.Name = "Agent";
            this.Agent.ReadOnly = true;
            // 
            // ReceivedDateTime
            // 
            this.ReceivedDateTime.DataPropertyName = "RecievedDateTime";
            this.ReceivedDateTime.HeaderText = "ReceivedDateTime";
            this.ReceivedDateTime.Name = "ReceivedDateTime";
            this.ReceivedDateTime.ReadOnly = true;
            // 
            // IGPNo
            // 
            this.IGPNo.DataPropertyName = "IGP_No";
            this.IGPNo.HeaderText = "IGPNo";
            this.IGPNo.Name = "IGPNo";
            this.IGPNo.ReadOnly = true;
            // 
            // NoOfRolls
            // 
            this.NoOfRolls.DataPropertyName = "NoOfRolls";
            this.NoOfRolls.HeaderText = "NoOfRolls";
            this.NoOfRolls.Name = "NoOfRolls";
            this.NoOfRolls.ReadOnly = true;
            // 
            // Weight
            // 
            this.Weight.DataPropertyName = "Weight";
            this.Weight.HeaderText = "Weight";
            this.Weight.Name = "Weight";
            this.Weight.ReadOnly = true;
            // 
            // Color
            // 
            this.Color.DataPropertyName = "Color";
            this.Color.HeaderText = "Color";
            this.Color.Name = "Color";
            this.Color.ReadOnly = true;
            // 
            // InProcess
            // 
            this.InProcess.DataPropertyName = "InProcess";
            this.InProcess.HeaderText = "InProcess";
            this.InProcess.Name = "InProcess";
            this.InProcess.ReadOnly = true;
            // 
            // IsOut
            // 
            this.IsOut.DataPropertyName = "IsOut";
            this.IsOut.HeaderText = "IsOut";
            this.IsOut.Name = "IsOut";
            this.IsOut.ReadOnly = true;
            // 
            // ViewOrder
            // 
            this.ViewOrder.HeaderText = "Go In process";
            this.ViewOrder.Name = "ViewOrder";
            this.ViewOrder.ReadOnly = true;
            this.ViewOrder.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ViewOrder.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ViewOrder.Text = "Go Inprocess";
            this.ViewOrder.UseColumnTextForButtonValue = true;
            // 
            // GoOrderOut
            // 
            this.GoOrderOut.HeaderText = "Go Order Out";
            this.GoOrderOut.Name = "GoOrderOut";
            this.GoOrderOut.ReadOnly = true;
            this.GoOrderOut.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.GoOrderOut.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.GoOrderOut.Text = "Go Order Out";
            this.GoOrderOut.UseColumnTextForButtonValue = true;
            // 
            // Order_List_Recceptionist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1165, 526);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.MiniMize_BUnton);
            this.Controls.Add(this.Clode_Button);
            this.Controls.Add(this.Title);
            this.Controls.Add(this.Logo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Order_List_Recceptionist";
            this.Text = "Order_List_Recceptionist";
            this.Load += new System.EventHandler(this.Order_List_Recceptionist_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label MiniMize_BUnton;
        private System.Windows.Forms.Label Clode_Button;
        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Panel Logo;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Customer_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Agent;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReceivedDateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn IGPNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoOfRolls;
        private System.Windows.Forms.DataGridViewTextBoxColumn Weight;
        private System.Windows.Forms.DataGridViewTextBoxColumn Color;
        private System.Windows.Forms.DataGridViewTextBoxColumn InProcess;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsOut;
        private System.Windows.Forms.DataGridViewButtonColumn ViewOrder;
        private System.Windows.Forms.DataGridViewButtonColumn GoOrderOut;
    }
}